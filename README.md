## Eui（原Eadmin）开箱即用的后台UI框架更新到了 v3.0版本

依旧保持原生JS开发，开箱即用，无需打包环境，写法与原生HTML几乎一致。为不熟悉mvvm模式的开发人员提供了多一种的选择。

#### (现在开源的是2.0版本，v3.0新版本请查看官网授权说明)

[官网](http://www.eui6.com)

交流QQ群：736885993

 **新版本界面预览图如下：** 

![输入图片说明](dist/static/img/QQ%E5%9B%BE%E7%89%8720220914114617.jpg)
![输入图片说明](dist/static/img/0e8c283c057c4e69937fb7375602f2c2_tplv-obj.png)
![输入图片说明](dist/static/img/3abb0f3ee10346fd8f39e9c06506f9ed_tplv-obj.png)
![输入图片说明](dist/static/img/13c6d21b88e4480ead1617c8ebf8fa82_tplv-obj.png)
![输入图片说明](dist/static/img/45955d2a2e944a30a3b6aea4d1d0cbec_tplv-obj.png)
![输入图片说明](dist/static/img/64569046530746fa9efef3b1e579f8f8_tplv-obj.png)
![输入图片说明](dist/static/img/a2c1832ad5fc48cdb321f483889a6449_tplv-obj.png)
![输入图片说明](dist/static/img/a7e6640c64c7412fb2b53fe84baf701f_tplv-obj.png)
![输入图片说明](dist/static/img/ac9836cd113a4bf98f1968c623e4efb1_tplv-obj.png)
![输入图片说明](dist/static/img/b6888c86583f400696a14173f886f9b7_tplv-obj.png)
![输入图片说明](dist/static/img/c725a59ff7504c948ebcaeaf56f13be8_tplv-obj.png)
![输入图片说明](dist/static/img/dc25c5a3131f4281b43316d1026ebc3e_tplv-obj.png)
![输入图片说明](dist/static/img/e49f36fb042b4825a2274b5ea9316f81_tplv-obj.png)
![输入图片说明](dist/static/img/e741643029844d07b2c87b842d213014_tplv-obj.png)
![输入图片说明](dist/static/img/fea1ea8970c44800bdf70e2958ee34a9_tplv-obj.png)